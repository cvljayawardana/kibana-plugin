import decryptAPI from './server/routes/decryptAPI';

export default function (kibana) {
  return new kibana.Plugin({
    require: ['elasticsearch'],
    name: 'decrypt',
    uiExports: {
      app: {
        title: 'Decrypt',
        description: 'DE FUDGING CRYPT !',
        main: 'plugins/decrypt/app',
      },
      hacks: [
        'plugins/decrypt/hack'
      ],
      styleSheetPaths: require('path').resolve(__dirname, 'public/app.scss'),
    },

    config(Joi) {
      return Joi.object({
        enabled: Joi.boolean().default(true),
      }).default();
    },

    init(server, options) { // eslint-disable-line no-unused-vars
      // Add server routes and initialize the plugin here
      decryptAPI(server);
    }
  });
}
