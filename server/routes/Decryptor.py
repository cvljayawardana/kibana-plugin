import base64
import hashlib
from Crypto import Random
from Crypto.Cipher import AES
import sys

sys.dont_write_bytecode = True
BS = 16
pad = lambda s: s + (BS - len(s) % BS) * chr(BS - len(s) % BS).encode()
unpad = lambda s: s[:-ord(s[len(s)-1:])]

def iv():
    """
    The initialization vector to use for encryption or decryption.
    It is ignored for MODE_ECB and MODE_CTR.
    """
    return chr[0] * 16


class AESCipher(object):


    def __init__(self, key):
        self.key = key
        #self.key = hashlib.sha256(key.encode()).digest()

    def decrypt(self, enc):
        encB64 = base64.b64decode(enc)
        varIv = iv()
        varKey =self.key
        cipher = AES.new(varKey, AES.MODE_CBC, varIv)
        dec = cipher.decrypt(encB64)
        return unpad(dec)



def sol(enc):
    # key = 'put your key here'
    dec = AESCipher(key).decrypt(enc)

    s = (dec)
    result = s[16:]
    return result 

#print sol('Ihb+pErlZlqlBYubYdC37wmyW2+cpfFoZ2CWXbz7qrVidO+adBPUsBnZix4mMeYWLlQmip6GqZV4wnMZhXIHZV2dzZrXBejjqQiXukf2Cl/ZjLSSDVKOZEeuLYBWWHBpOzz0x7kN5UswCQFYywIMz3rdcj994fSmJBGLvttnB2QQlNcxQJP+qm0poxh9pdBDeLU9QYGRsIg+5LEzlCDLWPZefWg+YbmY5vjnmNbFUI2OwYSk0NaNX2xb7Yv/oqmCqGwvv0u+bXaX7nj5jCK1sg==')		
