#!/usr/bin/python
import sys, getopt, time
import Decryptor as cipher

def main(argv):
    argument = ''
    result = ''
    
    # parse incoming arguments
    try:
        opts, args = getopt.getopt(argv,"hf:",["enc="])
    except getopt.GetoptError:
        print("salt@!@#$1234serverError")
        sys.exit(2)
    for opt, arg in opts:
        if opt in ("-f", "--enc"):
            argument = arg

    try:
        result = cipher.sol(argument)
    except:
        result = "salt@!@#$1234serverError"

    print(result)

if __name__ == "__main__":
    main(sys.argv[1:])