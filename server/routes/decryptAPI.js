import path from 'path';
import { spawnSync } from 'child_process';
import Constants from '../util/Constants';

export default function (server) {

  server.route({
    path: '/api/decrypt',
    method: 'POST',
    async handler(req,header) {
      var payload = req.payload.enc;
      
      const decryptedData = await getDecryptData(payload);
      const data = { decrypted: decryptedData }

      if(decryptedData.includes(Constants._SALT_SERVER)){
            return header.response(data).code(500);
      }
      
      return header.response(data).code(200);
    }
  });
  async function getDecryptData(encrypted){
    //spawns a synchronous child-process to interprit the python script 
    function runScript(){
      return spawnSync('python', [
        path.join(__dirname, 'Cipher.py'),
        "--enc", encrypted,
      ],{ encoding : 'utf8' });
    }
    
    const subprocess = runScript()
    return subprocess.stdout;
  }
}
