import React from 'react';
import ErrorMessage from '../util/ErrorMessageEnum';
import { 
    EuiForm,
    EuiFormRow,
    EuiButton,
    EuiFormControlLayout,
    EuiFlexGroup,
    EuiFlexItem,
    EuiIconTip
} from '@elastic/eui';

export class InputHandler extends React.Component {
    constructor(props){
        super(props);
        this.handleDecrypt = this.handleDecrypt.bind(this);
        this.handleInputChange = this.handleInputChange.bind(this);
        this.decryptData = this.decryptData.bind(this);
        this.state = { encryptText : '', decryptedText : '' , 
                       errorOccured : false, errorMsg:''};
    }

    handleInputChange (event) {
        this.setState({
            encryptText: event.target.value
        });
    }

    async handleDecrypt () {
        //text field empty
        if(!this.state.encryptText){
            this.setState({errorOccured:true,
                errorMsg:ErrorMessage.FIELD_EMPTY});
            this.props.onDecryption('', false); 
            return         
        }

        await this.decryptData();

        //internal server error 
        if(this.state.errorOccured){
            this.props.onDecryption('',false);
            return
        }

        this.props.onDecryption(this.state.decryptedText,true);
        this.setState({
            errorOccured:false,
            errorMsg:''
        })
    }

    async decryptData () {
        const {httpClient} = this.props;
        const params = {"enc":this.state.encryptText}
        await httpClient.post("../api/decrypt",params).then((resp) => {
            this.setState({decryptedText:resp.data.decrypted,
                            errorOccured:false});
        }).catch((error) => {
            this.setState({
                errorOccured:true,
                errorMsg:ErrorMessage.SERVER_ERROR
            });
        });
    }

    componentDidMount(){

    }

    render(){
        return(
            <EuiForm className = "inputContainer">
                <EuiFormRow label="Encrypted text" helpText="Paste the text to be decrypted here.">
                    <EuiFormControlLayout clear={{ onClick: () => {{this.setState({encryptText:""})}}}}>
                        <input type="text" className="euiFieldText" 
                               value = {this.state.encryptText} 
                               onChange = {this.handleInputChange}/>
                    </EuiFormControlLayout>

                </EuiFormRow>
                <EuiFlexGroup alignItems="center" gutterSize="s" responsive={false}>
                    <EuiFlexItem grow={false}>
                        <EuiButton type="submit" fill onClick={this.handleDecrypt}>
                            Decrypt
                        </EuiButton>
                    </EuiFlexItem>
                    {


                        this.state.errorOccured&&this.state.errorMsg===ErrorMessage.SERVER_ERROR&&
                        <EuiFlexItem grow={false}>
                            <EuiIconTip
                                aria-label="Warning"
                                color="danger"
                                type="alert"
                                content={this.state.errorMsg}
                                size = 'm'
                                position="right"/>
                        </EuiFlexItem>
                    }
                    {
                        this.state.errorOccured&&this.state.errorMsg===ErrorMessage.FIELD_EMPTY&&
                        <EuiFlexItem grow={false}>
                            <EuiIconTip
                                aria-label="Warning"
                                color="primary"
                                type="iInCircle"
                                size = 'm'
                                content={this.state.errorMsg}
                                position="right"/>
                        </EuiFlexItem>
                    }
                </EuiFlexGroup>
            </EuiForm>          
        );
    }
}