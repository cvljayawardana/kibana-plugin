import React from 'react';
import {
  EuiPage,
  EuiPageHeader,
  EuiPageBody,
  EuiPageContent,
  EuiPageContentBody
} from '@elastic/eui';
import { InputHandler } from '../InputHandler';
import { OutputHandler } from '../OutputHandler';

export class Main extends React.Component {
  constructor(props) {
    super(props);
    this.handleDecryption = this.handleDecryption.bind(this);
    this.state = {
        decryptedText : '',
        doShowResult : false
    }
  }

  handleDecryption(result, showResult){
    this.setState({
      decryptedText: result,
      doShowResult: showResult
    });
  }

  componentDidMount() {
  
  }

render() {
  const { httpClient } = this.props;
    return (
      <EuiPage>
        <EuiPageBody className = "pageBodyCustom">
          <EuiPageHeader>
              <h1>Decryption Plugin</h1>
          </EuiPageHeader>
          <EuiPageContent className = "pageContentCustom">
            <EuiPageContentBody >
              <InputHandler onDecryption = {this.handleDecryption}  httpClient = {httpClient}/>
              {
                this.state.doShowResult &&
                <OutputHandler decryptedText = { this.state.decryptedText }/>
              }
            </EuiPageContentBody>
          </EuiPageContent>
        </EuiPageBody>
      </EuiPage>
    );
  }
}
