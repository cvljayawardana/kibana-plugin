import React from 'react';
import {
    EuiCode,
    EuiFlexGroup,
    EuiFlexItem
}from '@elastic/eui';

export class OutputHandler extends React.Component {
    constructor(props){
        super(props);
    }

    render(){
        return(
            <EuiFlexGroup className = "outputContainer" alignItems="center" gutterSize="s" responsive={false}>
                <EuiFlexItem grow={false}>
                    <span>
                        <p className="outputTitleText">Decrypted text</p>
                        <EuiCode className = "outputDecryptText">{this.props.decryptedText}</EuiCode>   
                    </span>
                </EuiFlexItem>
            </EuiFlexGroup>
        )
    }
}