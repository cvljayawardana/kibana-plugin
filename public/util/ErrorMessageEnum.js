var ErrorMessageEnum = {
    FIELD_EMPTY: "Input field can not be empty.",
    SERVER_ERROR: "Decryption failure - Invalid encrypted value"
};

export default ErrorMessageEnum;